# René Stalder – Renovate

> **Renovate**: Save time and reduce risk by automating dependency updates in software projects.

Renovate setup that updates dependencies in all defined client projects as maintenance service.

- See [Renovate Runner](https://gitlab.com/renovate-bot/renovate-runner/-/tree/master/) for more information about the setup itself.
- See [official Renovate website](https://www.whitesourcesoftware.com/free-developer-tools/renovate)

## Setup in a project

1. Add a Renovate config to the project you want to enable Renovate.
2. Update the Renovate repositories environment variable with the project

### Add a Renovate config to the project you want to enable Renovate

Example: `.gitlab/renovate.json`

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": ["gitlab>renestalder-hq/infrastructure/renovate//presets/default"]
}
```

### Update the Renovate repositories environment variable with the project

Add the project with the full namespace to the `RENOVATE_EXTRA_FLAGS` [environment variable](https://gitlab.com/renestalder-hq/infrastructure/renovate/-/settings/ci_cd).
